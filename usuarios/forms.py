from django import forms

from usuarios.models import Usuario


class FormUsuario(forms.ModelForm):

    class Meta:
        model = Usuario
        fields = ['login', 'email']

    def save(self, commit=True):
        usuario = super(FormUsuario, self).save(commit=False)
        usuario.set_password("123@mudar")
        if commit:
            usuario.save()
        return usuario


class FormAlteraUsuario(forms.ModelForm):

    class Meta:
        model = Usuario
        fields = ['login', 'password', 'email']
