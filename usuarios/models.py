from django.db import models
from django.contrib.auth.models import (AbstractBaseUser,
                                        BaseUserManager, PermissionsMixin)


class UserManager(BaseUserManager):

    def _create_user(login, password, **extra_fields):
        if not login:
            raise ValueError('Login deve ser declarado')
        user = self.model(login=login, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(login, password, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return _create_user(login, password, **extra_fields)

    def create_superuser(login, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        return _create_user(login, password, **extra_fields)


class Usuario(AbstractBaseUser, PermissionsMixin):

    login = models.CharField('Login', max_length=100, unique=True)
    email = models.CharField('E-mail', max_length=250, unique=True)
    is_staff = models.BooleanField('Faz parte da equipe?', default=False)
    is_superuser = models.BooleanField('É super usuário?', default=False)

    USERNAME_FIELD = 'login'
    REQUIRED_FIELDS = ['email']

    def __str__(self):
        return self.login
