from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from usuarios.forms import (FormUsuario, FormAlteraUsuario)
from usuarios.models import Usuario


class UsuarioAdmin(UserAdmin):

    form = FormAlteraUsuario
    fieldsets = ((None, {'fields': ('login', 'email', 'password')}),)
    add_form = FormUsuario
    add_fieldsets = ((None, {'fields': ('login', 'email')}),)
    list_display = ['login']
    filter_horizontal = []
    ordering = []
    list_filter = []


admin.site.register(Usuario, UsuarioAdmin)

# Register your models here.
