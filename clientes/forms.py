from django import forms

from clientes.models import Cliente


class FormCliente(forms.ModelForm):

    class Meta:
        model = Cliente
        fields = ['login', 'email', 'password', 'nome', 'sobrenome', 'estado',
                  'cidade', 'rua', 'numero']
