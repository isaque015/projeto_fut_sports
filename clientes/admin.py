from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from clientes.forms import FormCliente
from clientes.models import Cliente


class ClienteAdmin(UserAdmin):

    form = FormCliente
    fieldsets = ((None, {'fields': ('login', 'email', 'password', 'nome',
                  'sobrenome', 'estado', 'cidade', 'rua', 'numero')}),)
    list_display = ['login']
    filter_horizontal = []
    ordering = []
    list_filter = []


admin.site.register(Cliente, ClienteAdmin)

# Register your models here.
