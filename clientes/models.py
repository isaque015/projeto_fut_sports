from django.db import models

from usuarios.models import Usuario


class Cliente(Usuario):

    nome = models.CharField('Nome', max_length=100)
    sobrenome = models.CharField('Sobrenome', max_length=100)
    estado = models.CharField('Estado', max_length=2)
    cidade = models.CharField('Cidade', max_length=100)
    rua = models.CharField('Rua', max_length=450)
    numero = models.IntegerField('Número')
# Create your models here.
